﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
import re
import os

import pymorphy2
# import pymystem3

class WordsSplitter:
    """ """
    def __init__ (self, fn_stopwords=None):
        if fn_stopwords is None:
            self.fn_stopwords = os.path.dirname(os.path.realpath(__file__)) + "\\StopWords.txt"
        else:
            self.fn_stopwords = fn_stopwords
        
        with codecs.open (self.fn_stopwords, 'r', encoding='utf-8-sig') as f:
             self.stops = set([ line.rstrip().strip()  for line in f  if (len(line) > 3) ])

        self.re_extract_words = re.compile (ur'[^а-яёa-z0-9]')
        self.re_repeat_spaces = re.compile (ur'[ ]+')

        self.morph = pymorphy2.MorphAnalyzer ()
        # self.morph = pymystem3.Mystem ()

    def normalize (self, word):
        return self.morph.parse(word)[0].normal_form
    
    def tokenize (self, text):
        """ """
        ext_text = self.re_extract_words.sub(u' ', text.lower())
        ext_text = self.re_repeat_spaces.sub(u' ', ext_text)
        words = ext_text.split(u' ')
    
        norms = [ self.normalize(w)  for w in words  if (len(w) >= 3) and (w not in self.stops) ]
        # norms = [ w for w in self.morph.lemmatize (ext_text)  if (len(w) >= 3) and (w not in self.stops) ]
        return norms



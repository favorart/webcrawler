﻿import codecs
import sys
import os
import re

from SearchWords import WordsSplitter

FN_WORDS  = sys.argv[1]
FN_REXPS  = True
DATA_DIR  = "..\\deshan-simple-web-crawler\\save\\"
# RXP     = re.compile (r"((?:^|\t|href\s*=\s*').*страхов|мультиполис|выезжающий|за рубеж|защита путешествий|[cs]trak?[hx]ov.*(?:['\"]\s*>|$))")
RXP       = re.compile (ur'\s(эквайринг|эквайрингов|фискализац|фискальник|расчетн.*кассов.*обслуживан|автономн.*\sкасс|виртуальн.*\sкасс.*\sонлайн\s|документ.*\sвалютн.*\sконтрол|зарегистриров.*\sкассу|кассовый.*аппарат|кассовый.*принтер|касс.*\sонлайн\s.*\sналоговая|контрольно.*\sкассов(ое|ая).*(техника|оборудование)|(облачн|магазинн).*\sкасс|фискальн.*данн|офд\s|паспорт.*сделк|пос\s.*терминал|pos\s.*терминал|(платеж|расчет).*\s(юан[яиь]|Кита[ей])|рекуррентн.*платеж|фискальн.*принтер|чеков.*принтер|терминал.*\s(торговый|оплаты|приема.*\sплатежей|пластиков.*\sкарт))')

ws = WordsSplitter ()

def findPair (arr1, arr2, n): 
    """ """
    i, j = 0, 0
    # Search for a pair
    while i < len (arr1) and j < len (arr2):
        if  abs (arr1[j] - arr2[i]) <= n:
            return True   # (i, j) if i < j else (j, i)
        elif arr1[j] < arr2[i]:
            j += 1
        else:
            i += 1
    return False


def search_words (ws, text, q_norms, n):
    """ """
    t_norms = ws.tokenize (text)

    dict_norms = {}
    for i, norm in enumerate (t_norms):
        if  dict_norms.has_key (norm):
            dict_norms[norm].append (i)
        else:
            dict_norms[norm] = [ i ]

    res = []
    for q_norm in q_norms:
        indices = []
        # for norm in q_norm:
        #     if  not dict_norms.has_key (norm):
        #         return None
        #     else:
        #         indices.append (dict_norms[norm])
        # 
        # for i in xrange (len (indices) - 1):
        #     # res = findPair (indices[i], indices[i+1])
        #     # if  res is None: break
        #     if not findPair (indices[i], indices[i+1]):
        #         break
        # else:
        #     res += q_norm
        if  dict_norms.has_key (q_norm):
            res.append (q_norm)

    return  res


if  __name__ == '__main__':

    if  not FN_REXPS:
        q_norms = []
        with codecs.open (FN_WORDS, 'r', encoding='utf-8-sig') as f:
            # q_norms = [ ws.tokenize (query)  for query in f.read ().splitlines () ]
            for query in f.read ().splitlines ():
                q_norms += ws.tokenize (query)
        q_norms = sorted (set (q_norms))

    domains = set()
    table = {}
    count = 1

    # traverse root directory, and list directories as dirs and files as files
    for root, dirs, files in os.walk (DATA_DIR):
        root_path = root.split (os.sep)
        # print ((len (root_path) - 1) * '---', os.path.basename (root))
    
        if  not root_path[-1]:
            continue
        elif    root_path[-1] == 'files':
            domain = root_path[-2]
        else:
            domain = root_path[-1]

        outstr = '%d\t%s' % (count, domain)
        print outstr, ' ' * (70 - len (outstr)), '\r',
        count += 1
        # if count == 25: break

        for file in files:
            # print (len(path) * '---', file)
            file_path = os.path.join (root, file)
            if  os.path.isfile (file_path):
                with open (file_path) as fin:
                    text = fin.read ()

                    # if  domain not in domains:
                    if  FN_REXPS:
                        searched = RXP.search (text)
                        if  searched is not None:
                            # if  root_path[-1] == 'files':
                            table[domain.replace ('www.', '')] = searched.group (1)                    
                            domains.add (domain)
                    else:
                        searched = search_words (ws, text, q_norms, 5)
                        if  searched:
                            table[domain.replace ('www.', '')] = ' '.join (searched)
                            domains.add (domain)

    texts = [ unicode('%s\t%s' % (domain, text.rstrip ()), 'utf-8', 'ignore')  \
              for domain, text in table.items ()                               \
            ]

    with codecs.open ('find-sence-words.txt', 'w', encoding='utf-8-sig') as fout:
        print >>fout, u'\r\n'.join (sorted (texts)),

    with codecs.open ('found-sence-domains.txt', 'w') as fout:
        print >>fout, '\r\n'.join (sorted (domains)),


﻿# import __future__
from __future__ import division

from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool

import codecs
import sys
import re
import os

import crawler


DOMAINS_DIR = sys.argv[1]
CRAWL_DEPTH = sys.argv[2] # how deep web page should be crawled
VERBOSE = False
IS_SINGLE_THREAD = True


def crawl_url (url):
    """ """
    cr = crawler.Crawler (url, CRAWL_DEPTH, saveTitles=True)
    cr.start (verbose=VERBOSE)
    cr.dumpTitles ()
    cr.dumpCrawled ()
    return


if  __name__ == '__main__':

    fns = [ os.path.join (DOMAINS_DIR, fn)                         \
            for fn in os.listdir (DOMAINS_DIR)                     \
            if  os.path.isfile (os.path.join (DOMAINS_DIR, fn))    \
            and fn.endswith ('.txt')                               \
          ]
    
    lines = []
    for fn in fns:
        with codecs.open (fn, 'r', encoding='utf-8-sig') as f:
            lines += f.read ().splitlines ()
    
    urls = [ 'http://www.' + str (line.split ()[0].encode("idna")) + '/'   \
              for line in lines                                            \
              if  len (line.split ()   ) >= 1                              \
              and len (line.split ()[0]) >= 5                              \
           ]
    
    if  IS_SINGLE_THREAD: 
        i = 1
        for url in urls: 
            # print seedUrl
            crawl_url (url)
            if i % 10:  print '\r%d  ' % i, 
            i += 1
    else:
        # Then MULTI-THREAD
        pool = ThreadPool (14)
        pool.map (crawl_url, urls)
            
        try:
            for i, _ in enumerate (pool.imap_unordered (crawl_url, urls), 1):
                sys.stdout.write ('\rdone {0:%}'.format (i / len (urls)) )
            print
        except Exception as msg:
            print msg
    
        pool.close()
        pool.join()

    
# # extract mails
# import MailExtracter
# exMails = False
# if  exMails:
#     me=MailExtracter.MailExtracter (path)
#     me.start()
#     me.printMailList()

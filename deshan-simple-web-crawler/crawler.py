﻿import httplib
import urllib2
import socket
import codecs
import uuid
import sys
import re

from urlparse import urlparse
from os.path import join, exists
from bs4 import BeautifulSoup, SoupStrainer

import crawlerSettings
import crawlerFiles

# linkRgx = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', re.IGNORECASE)
mimeTypeRgx = re.compile(r'^[A-Z,a-z]+[A-Z,a-z,1-9,]*/[A-Z,a-z,1-9]+')


class Crawler:
          
    def __init__ (self, seed, depth, saveTitles, externalLinks=False):
        parseResult = urlparse (seed)
        self.domain = parseResult.netloc
        self.basePath = join (crawlerSettings.BASE_SAVE_PATH, (self.domain + '').replace ('www.', ''))
        self.fileSavePath = join (self.basePath, crawlerSettings.DEFAULT_FILE_FOLDER)
        self.externalLinks = externalLinks
        self.saveTitles = saveTitles
        self.depth = int (depth)
        self.seed = seed

    def __downloadFile (self, url, urlPath):
        # res = urllib2.urlopen (url, timeout=1)
        try:
            self.conn.request ("GET", urlPath)
            data = self.conn.getresponse ().read ()
        except socket.error as msg:
            print >>sys.stderr, '__downloadFile: self.conn.request ("GET", %s)\t%s' % (self.domain, msg)
            return None
                
        filename = uuid.uuid4 ()
        crawlerFiles.saveFile (data, str (filename), self.fileSavePath)
        return data

    def __harvestLinks (self, text, verbose, verbose_links):
        links = set ()
        title = ''
        if  text: # is not None:
            bs = BeautifulSoup (text, "lxml") # , parse_only=SoupStrainer('a', href=True))
            found = [ link['href']  for link in bs.find_all("a", href=True) ]
            title = bs.title.string  if (bs.title) else  ''

            if  not self.externalLinks:
                internals = []
                for url in found:
                    if   url.startswith  ('/'):
                        internals.append (self.seed + url[1:])
                    elif url.startswith  (self.seed):
                        internals.append (url)
                    elif not url.startswith  ('http://'):
                        internals.append (self.seed + url)

                if  verbose:
                    externals = list (set (found) - set (internals))
                    if  len (externals) > 0:
                        print  "harvestLinks"
                        print  "Externals count: " + str (len (externals))
                        if  verbose_links:
                            print '[\t' + '\n\t'.join (externals[:10]) + "\n]\n\n"
                links.update (internals)
            else:
                links.update (found)
        return links, title
    
    def __isValidLink (self, url, verbose):
        urlParse = urlparse (url)
        try:
            self.conn.request ("GET", urlParse.path)
            res = self.conn.getresponse ()
            res.read ()
        except socket.error as msg:
            print >>sys.stderr, '__isValidLink: self.conn.request ("GET", %s)\t%s' % (self.domain, msg)
            return None

        if  res is not None and str (res.status) in crawlerSettings.STATUS_CODES:
            head = res.getheader ("Content-Type")
            if  head is not  None:
                mimeType = mimeTypeRgx.match (head).group (0)
                if  verbose: print '\tContent type: ' + mimeType 
                if  mimeType in crawlerSettings.VALID_MIME_TYPES:
                    return urlParse.path
        return None
        
    def __crawl (self, urls_list, depth, verbose, verbose_links):
        toCrawl = set ()
        toCrawl.update (urls_list)
        newURLs = set ()
        count = 0
        
        if  verbose:
            print '==============================================================================='
            print 'Depth : ' + str (depth)
            print '==============================================================================='
        while toCrawl:
            try:
                url = toCrawl.pop ().encode ('ascii', 'replace') # str ()
            except Exception as msg:
                print >>sys.stderr, '__crawl: UnicodeError link: %s\t%s' % (url, msg)
                continue
                
            count += 1
            if verbose: print '[%d]------------------------------------------' % (count)
            if 1:
            # try:
                if verbose:
                    print 'Checking Link: ' + url + ' ...'               
                print >>sys.stdout, '[%d] done [%d/%d] depth [%d] rest toCrawl %s   \r' \
                       % (count, (self.depth - depth) + 1, self.depth + 1, len (toCrawl), self.seed),

                urlPath = self.__isValidLink (url, verbose)
                if  urlPath is not None:
                    if verbose: print '\tCrawling...'
                    if verbose: print '\tDownloading..'
                    content = self.__downloadFile (url, urlPath)
                    self.crawled.add (url)
                    if verbose: print '\tDownloading completed'
                    if verbose: print '\tHarvesting links...'
                    foundURLs, title = self.__harvestLinks (content, verbose, verbose_links)
                    self.titles[url] = title
                    foundURLs -= self.crawled
                    newURLs.update (foundURLs)
                    if verbose: print '\tHarvesting links completed'
                    if verbose: print '\tCrawling completed - ' + str (len (foundURLs)) + ' links found'
                    if verbose_links: 
                        print '[\t' + '\n\t'.join (foundURLs) + "\n]\n\n"
                    
                else:
                    if verbose: print 'Skipped Error Url: ' + url
                    continue
            # except Exception as msg:
            #     print >>sys.stderr, '__crawl: Crawling Error link: %s\t%s' % (url, msg)

        if  len (newURLs) and 0 < depth:
            self.__crawl (newURLs, depth=depth-1, verbose=verbose, verbose_links=verbose_links)  
        else:
            return 
              
    def start (self, verbose=False, verbose_links=False):
        if  verbose:
            print 'Crawling started   ', self.seed
            # print 'Files are saved at path : ' + self.fileSavePath
        
        try:
            self.conn = httplib.HTTPConnection (self.domain, 80, timeout=1)
        except socket.error as msg:
            print >>sys.stderr, "self.conn = httplib.HTTPConnection (%s)\t%s" % (self.domain, msg)
            return

        self.titles = {}
        self.crawled  = set ()
        self.__crawl ([self.seed], self.depth, verbose, verbose_links)
        self.conn.close () 
            
    def dumpTitles (self):
        if  exists (self.basePath):
            filename = self.domain + '-titles.txt'
            with codecs.open (join (self.basePath, filename), 'a') as f_out: # , encoding='utf-8'
                for url, title in self.titles.items ():
                    print >>f_out, "%s\t%s\r\n" % (url  .encode ('utf-8', 'ignore') if url   is not None else 'None', 
                                                   title.encode ('utf-8', 'ignore') if title is not None else 'None'),
        return

    def dumpCrawled (self):
        if  exists (self.basePath):
            filename = self.domain + '-crawled.txt'
            with codecs.open (join (self.basePath, filename), 'a') as f_out: # , encoding='utf-8'
                 print >>f_out, '\r\n'.join (sorted (self.crawled))
        return


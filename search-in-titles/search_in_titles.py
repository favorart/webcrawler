#!/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
import shutil
import json
import mmap
import sys
import re
import os
import io

from collections import defaultdict
from operator import itemgetter

from SearchWords import WordsSplitter

if  len(sys.argv) < 4:
    raise ValueError('Not enough arguments')

FN_SIMIL    = sys.argv[1]
FN_INDEX    = sys.argv[2]
FN_PHRASES  = sys.argv[3]

DOMAINS_DIR = 'domains'
FN_OUTPUT   = 'output.txt'
# ==============================================================================
def context2_index_read (fn_index):
    """ Return dict. """
    index = {}
    print 'Context2 read index...'
    with open(fn_index, 'r') as f_index:
        for line in f_index:
            splt = line.split()
            index[splt[0]] = (int(splt[1]), int(splt[2]))
    print 'done.\n'
    return index

# ==============================================================================
def find_ngrams(input_list, n):
    """ http://locallyoptimal.com/blog/2013/01/20/elegant-n-gram-generation-in-python/ """
    return zip(*[input_list[i:] for i in range(n)])


def prepare_phrases (fn_phrases, word_splitter, n=0):
    """ 
        Params:
            fn_phrases     - file name with phrases, \n separated
            word_splitter  - 
            n              - 0 : combinations as is
                             1 : word searching
                             2 : 2gramms search
                             3 : 3gramms search
                             ...
    """
    with codecs.open (fn_phrases, 'r', encoding='utf-8-sig') as f:
        phrases = [ word_splitter.tokenize (phrase)  for phrase in f.read ().splitlines ()  if len (phrase) > 3 ]

    if  n > 0:
        res = find_ngrams (phrases, n)
    else:
        res = [ p for p in phrases if len (p) > 1 ]
    return res

# ==============================================================================
def search_in_titles (norms, phrases):
    """ """
    norms_set = set (norms)
    res = []
    for phrase in phrases:
        for norm in phrase:
            if  norm not in norms_set:
                break
        else: res.append ( u' '.join (phrase) ) #, u' '.join (norms)) )
    return u'  '.join (res) if res else None

def extract_and_search_in_all_titles (fn_simil, index, phrases, ws, verbose=False):
    """ """
    if verbose: print 'Extract and searching in all titles ...' 
    res = []
    
    i = 0
    with open(fn_simil, 'rb') as f:
         mm = mmap.mmap (f.fileno(), 0, access=mmap.ACCESS_READ)

         items = index.items ()  #[:5000]
         for domain, borders in items:

             # if  index.has_key (domain[0]):
             offsen_s, offset_l = borders         
             for line in mm[offsen_s:offset_l].splitlines ():

                 splt = line.split('\t')
                 if  len (splt) == 3:

                     # Prev Format :::
                     # Hadoop /data/laboratory/search_url_title.gz: 
                     # <url, список тайтлов на этом урле, количество людей>
                     url, titles, other = splt
                     context = json.loads (titles)
                     if  context:
                         norms = ws.tokenize (u'\r\n'.join (context))
                         # print >>sys.stderr, '%s' % line.encode('cp866', 'ignore')

                         what = search_in_titles (norms, phrases)
                         if what is not None:
                            # print what.encode ('cp866')
                            # raw_input ()
                            res.append (u'\t'.join ([url, what]))
             if  verbose: 
                 if  not (i % 100):
                     print "%d / %d domains done,  found %d.\r" % (i, len (items), len (res)),
                 i += 1

    if verbose:  print 'done.                                   \n'
    return res

# ==============================================================================
if __name__ == '__main__':
    
    ws = WordsSplitter ()
    
    phrases = prepare_phrases (FN_PHRASES, ws, n=0)
    # for phrase in phrases:
    #     print ' '.join (phrase).encode ('cp866')
    index = context2_index_read (FN_INDEX)
        
    res = extract_and_search_in_all_titles (FN_SIMIL, index, phrases, ws, verbose=True)

    with codecs.open (FN_OUTPUT, 'w', encoding='utf-8-sig') as f:
         print >>f, u'\r\n'.join (res),
